import 'package:flutter/material.dart';

class MyButton extends StatefulWidget {
  final String title;
  final Color warna;
  final Function()? myevent;

  MyButton({Key? key, required this.title, required this.warna, this.myevent}) : super(key: key);

  @override
  State<MyButton> createState() => _MyButtonState();
}

class _MyButtonState extends State<MyButton> {
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: widget.myevent,
      child: Text(widget.title),
      style: ElevatedButton.styleFrom(
        primary: widget.warna
      ),
    );
  }
}
