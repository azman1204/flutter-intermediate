import 'package:flutter/material.dart';
import 'package:get/get.dart';

main() {
  runApp(GetMaterialApp(
    home: GetxDemo(),
  ));
}

class GetxDemo extends StatelessWidget {
  const GetxDemo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('GetX Demo'),),
      body: Column(
        children: [
          ElevatedButton(onPressed: () {
            Get.defaultDialog(title: "Hello World");
          }, child: Text('Show Dialog'))
        ],
      ),
    );
  }
}
