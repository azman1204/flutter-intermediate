import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

main() {
  PostHelper ph = PostHelper();
  ph.getConnection();
  print('berjaya');
}

class PostHelper {
  Future<Database> getConnection() async {
    var databasesPath = await getDatabasesPath();
    print(databasesPath);
    String path = join(databasesPath, 'posts.db');
    Database database = await openDatabase(path, version: 1,
    onCreate: (Database db, int version) async {
      await db.execute(
          'CREATE TABLE posts ('
              'id INTEGER PRIMARY KEY AUTOINCREMENT, '
              'title TEXT, '
              'author TEXT)');
    });
    return database;
  }

  insertPost() async {
    Database db = await getConnection();
    int rs = await db.insert('posts', { 'title' : 'test 1', 'author': 'John Doe' });
    print('result = $rs');
  }

  updatePost() async {
    Database db = await getConnection();
    // UPDATE posts SET author = 'Abu' WHERE id = 1
    int rs = await db.update('posts', {'author':'Abu'},
        where: 'id=?', whereArgs: [1]);
    print('result = $rs');
  }

  updatePost2(Map<String, Object> post, int id) async {
    Database db = await getConnection();
    int rs = await db.update('posts', post, where: 'id=?',
        whereArgs: [id]);
    print('result = $rs');
  }

  Future<List> findAllPost() async {
    Database db = await getConnection();
    List posts = await db.query('posts');
    for (int i=0; i < posts.length; i++) {
      print(""
          "id = ${posts[i]['id']} "
          "title = ${posts[i]['title']} "
          "author = ${posts[i]['author']}");
    }
    return posts;
  }

  deletePost(int id) async {
    Database db = await getConnection();
    int rs = await db.delete('posts', where: 'id=?', whereArgs: [id]);
    print("resutl = $rs");
  }
}