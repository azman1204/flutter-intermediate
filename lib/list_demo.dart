import 'package:flutter/material.dart';
import 'package:flutter_intermediate/MyList.dart';

class ListDemo extends StatelessWidget {
  const ListDemo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('List Demo')),
      body: MyList(data: ['test1', 'test2'],),
    );
  }
}
