import 'dart:math';

import 'package:flutter/material.dart';
class RandomNo extends StatefulWidget {
  const RandomNo({Key? key}) : super(key: key);

  @override
  State<RandomNo> createState() => _RandomNoState();
}

class _RandomNoState extends State<RandomNo> {
  int randNo = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Random No'),),
      body: Row(
        children: [
          TextButton(
            onPressed: () {
              Random rnd = Random();
              setState(() {
                randNo = rnd.nextInt(100);
              });
            },
            child: Text('Generate Random No'),
          ),
          Text('Random No = $randNo')
        ],
      ),
    );
  }
}
