import 'package:flutter/material.dart';

class FutureDemo extends StatefulWidget {
  const FutureDemo({Key? key}) : super(key: key);

  @override
  State<FutureDemo> createState() => _FutureDemoState();
}

class _FutureDemoState extends State<FutureDemo> {
  @override
  void initState() {
    super.initState();
    test(); // 5 seconds utk complete
    print('Heloo');
  }

  void test() async {
    String cat = await Future.delayed(Duration(seconds: 3), () {
      print('cat');
      return 'cat';
    });

    String dog = await Future.delayed(Duration(seconds: 2), () {
      print('dog');
      return 'dog';
    });

    print("$cat - $dog");
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
