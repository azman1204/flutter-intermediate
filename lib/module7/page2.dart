
import 'package:flutter/material.dart';

class Page2 extends StatelessWidget {
  const Page2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final arg = ModalRoute.of(context)!.settings.arguments;
    print('data = ' + arg.toString());
    return (
      Scaffold(
        appBar: AppBar(
          title: Text('Page 2'),
        ),
        body: Row(
          children: [
            ElevatedButton(
              child: Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            SizedBox(width: 10.0,),
            ElevatedButton(
              child: Text('Go to page 3'),
              onPressed: () {
                Navigator.of(context).pushNamed('/page3');
              },
            ),
          ],
        ),
      )
    );
  }
}
