import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

// widget default
class Page3 extends StatefulWidget {
  const Page3({Key? key}) : super(key: key);

  @override
  State<Page3> createState() => _Page3State();
}

// data ada disini
class _Page3State extends State<Page3> {
  // state
  final arr = [
    'test 1',
    'test 2',
    'test 3'
  ];
  // state
  var posts = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print('running in initState...');
    getData();
  }

  getData() async {
    var url = Uri.https('jsonplaceholder.typicode.com', '/posts');
    var response = await http.get(url);
    List posts2 = convert.jsonDecode(response.body);
    print(posts2.length);
    setState(() {
      posts = posts2;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Page 3'),
      ),
      body: ListView(
        children:
          [
            ...posts.map((post) => Card(
              child: ListTile(
                leading: Text(post['id'].toString()),
                title: Text(post['title']),
                subtitle: Text(post['body']),
              ),
            ))
          ],
        ),
      );
  }
}
