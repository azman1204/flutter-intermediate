import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class MyPost extends StatefulWidget {
  const MyPost({Key? key}) : super(key: key);

  @override
  State<MyPost> createState() => _MyPostState();
}

class _MyPostState extends State<MyPost> {
  var posts = [];
  bool showList = true;
  var myTitle = TextEditingController();
  var myAuthor = TextEditingController();
  var id = 0;

  @override
  void initState() {
    super.initState();
    getData();
  }

  getData() async {
    var url = Uri.http('192.168.137.1:3000', '/posts');
    var response = await http.get(url);

    List posts2 = convert.jsonDecode(response.body);
    print(posts2.length);
    setState(() {
      posts = posts2;
    });
  }

  deleteData(var post) async {
    print(post['id']);
    var url = Uri.http('192.168.137.1:3000', '/posts/${post['id']}');
    var response = await http.delete(url);
    print(response.body);
    getData();
  }

  List<Widget> postList() {
    return posts.map((post) => (
      Card(
        child: ListTile(
          title: Text(post['title']),
          subtitle: Text(post['author']),
          trailing: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              IconButton(onPressed: () {
                setState(() {
                  print('author = ${post['author']}');
                  myTitle.text = post['title'];
                  myAuthor.text = post['author'];
                  id = post['id'];
                  showList = false;
                });
              }, icon: Icon(Icons.edit)),
              IconButton(
                onPressed: () {
                  deleteData(post);
                },
                icon: Icon(Icons.delete),
              ),
            ],
          ),
        ),
      )
    )).toList();
  }

  Widget showForm() {
    return Column(
      children: [
        TextField(
          controller: myTitle,
          onChanged: (val) {
            print(val);
            setState(() {
              //title = val;
            });
          },
          decoration: const InputDecoration(
              hintText: 'Title',
              border: OutlineInputBorder()
          ),
        ),
        const SizedBox(height: 10.0,),
        TextField(
          controller: myAuthor,
          onChanged: (val) {
            setState(() {
              //author = val;
            });
          },
          decoration: const InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'Author'
          ),
        ),
        ElevatedButton(
          child: const Text("Submit"),
          onPressed: () {
            updateData();
            // update data
            setState(() {
              showList = true;
            });
          },
        ),
      ],
    );
  }

  void updateData() async {
    print('updating data...');
    var url = Uri.http('192.168.137.1:3000', '/posts/$id');
    var response = await http.patch(url, body: {'author': myAuthor.text, 'title':myTitle.text});
    print('updating data ${response.body}');
    showList = true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('My Post'),
      ),
      body: ListView(
        children: [
          ...(showList ? postList() : [showForm()])
        ],
      ),
    );
  }
}

// ... spread operator, copy list. list1 = [1,2,3]; list2 = [...list1,4,5,6]
