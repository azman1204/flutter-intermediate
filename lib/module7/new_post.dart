import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class NewPost extends StatefulWidget {
  const NewPost({Key? key}) : super(key: key);

  @override
  State<NewPost> createState() => _NewPostState();
}

class _NewPostState extends State<NewPost> {
  String title = '';
  String author = '';
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('New Post'),),
      body: Column(
        children: [
          TextField(
            onChanged: (val) {
              print(val);
              setState(() {
                title = val;
              });
            },
            decoration: const InputDecoration(
              hintText: 'Title',
              border: OutlineInputBorder()
            ),
          ),
          const SizedBox(height: 10.0,),
          TextField(
            onChanged: (val) {
              setState(() {
                author = val;
              });
            },
            decoration: const InputDecoration(
                border: OutlineInputBorder(),
              hintText: 'Author'
            ),
          ),
          ElevatedButton(
            child: const Text("Submit"),
            onPressed: () {
              postData();
            },
          ),
          Text('$title / $author')
        ],
      ),
    );
  }
  // gitlab.com/azman1204
  postData() async {
    var url = Uri.http('192.168.178.76:3000', '/posts');
    var response = await http.post(url, body: {'author': author, 'title':title});
    print(response.body);
    Navigator.of(context).pushNamed('/my-post');
  }
}
