
import 'package:flutter/material.dart';

class Page1 extends StatelessWidget {
  const Page1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Page 1'),
        ),
        body: Column(
          children: [
            ElevatedButton(
              child: Text('Go to page 2'),
              onPressed: () {
                Navigator.of(context).pushNamed('/page2',
                    arguments: {'name':'John Doe', 'age': 40});
              },
            ),
            ElevatedButton(
              child: Text('Go to My Post'),
              onPressed: () {
                Navigator.of(context).pushNamed('/my-post');
              },
            ),
            ElevatedButton(
              child: Text('New Post'),
              onPressed: () {
                Navigator.of(context).pushNamed('/new-post');
              },
            ),
          ],
        ),
      ),
    );
  }
}
