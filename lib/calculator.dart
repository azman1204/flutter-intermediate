import 'package:flutter/material.dart';
import 'package:flutter_intermediate/guess_game.dart';

class Calculator extends StatefulWidget {
  const Calculator({Key? key}) : super(key: key);

  @override
  State<Calculator> createState() => _CalculatorState();
}

class _CalculatorState extends State<Calculator> {
  var myInvestment = TextEditingController();
  var myDuration = TextEditingController();
  var myRate = TextEditingController();
  var results = [];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          bottom: const TabBar(
            tabs: [
              Tab(icon: Icon(Icons.calculate),),
              Tab(icon: Icon(Icons.account_box),),
              Tab(icon: Icon(Icons.bus_alert),),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            ListView(
              children: [
                ...showForm(),
                ...showResult(),
              ],
            ),
            GuessGame(),
            Text('coming soon..')
          ],
        ),
      ),
    );
  }

  List<Widget> showForm() {
    return [
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: TextField(
          controller: myInvestment,
          decoration: InputDecoration(
            label: Text('Investment')
          ),
        ),
      ),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: TextField(
          controller: myDuration,
          decoration: InputDecoration(
            label: Text('Duration')
          ),
        ),
      ),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: TextField(
          controller: myRate,
          decoration: InputDecoration(
            label: Text('Profit Rate')
          )
        ),
      ),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: ElevatedButton(onPressed: () {
          calculate();
        }, child: Text('Submit')),
      )
    ];
  }

  void calculate() {
    int investment = int.parse(myInvestment.text);
    int duration = int.parse(myDuration.text);
    int rate = int.parse(myRate.text);
    var arr = [];
    int year = 2023;
    for(int i=0; i<duration; i++) {
      int profit = (investment * rate / 100).toInt();
      Result rs = Result(investment: investment, profit: profit, year: year);
      arr.add(rs);
      investment += profit;
      year++;
      print(rs.toString());
    }
    print(arr.toString());
    setState(() {
      results = arr;
    });
  }

  List<Widget> showResult() {
    return
      results.map((rs) {
        return Card(
          color: Colors.grey,
          child: ListTile(
            title: Text(rs.year.toString()),
            subtitle: Text('Investment : ${rs.investment}'),
            trailing: Text('Profit : ${rs.profit}'),
            leading: IconButton(icon: Icon(Icons.delete), onPressed: () {
              setState(() {
                results.remove(rs);
              });
            },),
          ),
        );
      }).toList()
    ;
  }
}

class Result {
  int year;
  int investment;
  int profit;

  Result({required this.investment, required this.profit, required this.year});

  @override
  String toString() {
    return '$investment $profit $year';
  }
}