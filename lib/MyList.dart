import 'package:flutter/material.dart';

class MyList extends StatefulWidget {
  final List data;
  const MyList({Key? key, required this.data}) : super(key: key);

  @override
  State<MyList> createState() => _MyListState();
}

class _MyListState extends State<MyList> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        ...widget.data.map((val) =>
          ListTile(
            title: Text(val),
          ),
        )
      ],
    );
  }
}
