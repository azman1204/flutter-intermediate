import 'dart:convert';
import 'package:http/http.dart' as http;

class TimeService {
  Future<String> getTime(String timezone) async {
    var uri = Uri.http('worldtimeapi.org', '/api/timezone/$timezone');
    var response = await http.get(uri);
    Map data = jsonDecode(response.body);
    String abbr = data['utc_offset'];
    int hour = int.parse(abbr.substring(0,3));
    DateTime now = DateTime.parse(data['datetime']);
    print('year = ${now.year} hour = ${now.hour + hour} minute = ${now.minute}');
    return '${now.hour + hour}:${now.minute}';
  }
}