import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_intermediate/world_time/time_service.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

class Loading extends StatefulWidget {
  const Loading({Key? key}) : super(key: key);

  @override
  State<Loading> createState() => _LoadingState();
}

class _LoadingState extends State<Loading> {
  @override
  void initState() {
    super.initState();
    getData();
  }

  void getData() async {
    TimeService ts = TimeService();
    String time = await ts.getTime('Asia/Kuala_Lumpur');
    Navigator.of(context).pushNamed('/worldtime-home',arguments: 'Kuala Lumpur $time');
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: SpinKitCircle(
        color: Colors.redAccent,
        size: 50.0,
      ),
    );
  }
}

