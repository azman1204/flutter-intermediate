import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  String time = '';

  @override
  Widget build(BuildContext context) {
    time = ModalRoute.of(context)!.settings.arguments as String;
    print("time = $time");
    return Scaffold(
      appBar: AppBar(
        title: Text('World Time'),
      ),
      body: Column(
        children: [
          Text("TIME : $time"),
          ElevatedButton(onPressed: () {
            Navigator.of(context).pushNamed('/choose-location');
          }, child: Text('Choose Other Location'))
        ],
      ),
    );
  }
}
