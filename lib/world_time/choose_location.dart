import 'package:flutter/material.dart';
import 'time_service.dart';

class Location extends StatefulWidget {
  const Location({Key? key}) : super(key: key);

  @override
  State<Location> createState() => _LocationState();
}

class _LocationState extends State<Location> {
  List countries = [
    {'city': 'Kuala Lumpur', 'url':'Asia/Kuala_Lumpur'},
    {'city': 'New York', 'url':'America/New_York'},
    {'city': 'London', 'url':'Europe/London'},
    {'city': 'Kenya', 'url':'Africa/Nairobi'},
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title:Text('ChooseCity'),),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
          children: [
            ...countries.map((country) =>
              OutlinedButton(onPressed: () async {
                TimeService ts = TimeService();
                print(country['url']);
                String time = await ts.getTime(country['url']);
                Navigator.of(context).pushNamed('/worldtime-home', arguments: '${country['city']} $time');
              }, child: Text(country['city']))
            )
          ],
        ),
      ),
    );
  }
}
