import 'dart:math';
import 'package:flutter/material.dart';

class GuessGame extends StatefulWidget {
  const GuessGame({Key? key}) : super(key: key);

  @override
  State<GuessGame> createState() => _GuessGameState();
}

class _GuessGameState extends State<GuessGame> {
  int randNo = 0;
  var myGuess = TextEditingController();
  String msg = '';
  int trials = 0;
  bool isWin = false;

  @override
  void initState() {
    super.initState();
    generateRandom();
  }

  void generateRandom() {
    Random rnd = Random();
    setState(() {
      randNo = rnd.nextInt(100);
    });
  }

  void playAgain() {
    myGuess.text = '';
    generateRandom();
    setState(() {
      isWin = false;
      trials = 0;
      msg = '';
    });

  }

  void showResult() {
    String msg2 = '';
    int guessNumber = int.parse(myGuess.text); // int.parse() convert text to int
    if (guessNumber == randNo) {
      // jawapan betul
      msg2 = 'You win after $trials trials';
      isWin = true;
    } else if (guessNumber > randNo) {
      msg2 = 'Guess Lower';
    } else {
      msg2 = 'Guess Higher';
    }

    setState(() {
      msg = msg2;
      trials++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Guess Game'),),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextField(
              controller: myGuess,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Guess a number'
              ),
            ),
            OutlinedButton(onPressed: (){
              showResult();
            }, child: Text('Go')),
            Text(msg),
            isWin ? ElevatedButton(
              onPressed: () {
                playAgain();
              },
              child: Text('Play Again ?'),
            ) :
            Text('wrong..$randNo')
          ],
        ),
      ),
    );
  }
}
