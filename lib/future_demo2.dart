// konsep async await - Future
// js - promise / observable (RxJS)
main() {
  test();
  print('Hello 1');
}

void test() async {
  String cat = await Future.delayed(Duration(seconds: 3), () {
    print('Hello 3');
    return "Cat";
  });

  String dog  = await Future.delayed(Duration(seconds: 2), () {
    print('Hello 4');
    return "Dog";
  });

  print('Hello $cat - $dog');
}

