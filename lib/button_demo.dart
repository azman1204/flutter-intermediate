import 'package:flutter/material.dart';
import 'package:flutter_intermediate/my_button.dart';

class ButtonDemo extends StatelessWidget {
  const ButtonDemo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title:Text('Button Demo')),
      body: Column(
        children: [
          MyButton(title: 'Klik Saya', warna: Colors.redAccent, myevent: () { print('Yo'); }),
          MyButton(title: 'More Button', warna: Colors.green, myevent: () { print('hi'); },),
          MyButton(title: 'More Button', warna: Colors.yellow,),
          MyButton(title: 'Play Game', warna: Colors.grey, myevent: () {
            Navigator.of(context).pushNamed('/guess-game');
          },),
        ],
      )
    );
  }
}
