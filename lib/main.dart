import 'package:flutter/material.dart';
import 'package:flutter_intermediate/button_demo.dart';
import 'package:flutter_intermediate/calculator.dart';
import 'package:flutter_intermediate/guess_game.dart';
import 'package:flutter_intermediate/list_demo.dart';
import 'package:flutter_intermediate/module7/my_post.dart';
import 'package:flutter_intermediate/module7/new_post.dart';
import 'package:flutter_intermediate/post_demo.dart';
import 'package:flutter_intermediate/random_no.dart';
import 'package:flutter_intermediate/future_demo.dart';
import 'package:flutter_intermediate/snackbar.dart';
import 'package:flutter_intermediate/spinkit_demo.dart';
import 'package:flutter_intermediate/world_time/choose_location.dart';
import 'package:flutter_intermediate/world_time/loading.dart';
import 'card_demo.dart';
import 'module7/page1.dart';
import 'module7/page2.dart';
import 'module7/page3.dart';
import 'world_time/home.dart';

void main() {
  runApp (
    MaterialApp(
      initialRoute: '/snackbar-demo' ,
      routes: {
        '/page1' : (context) =>  Page1(),
        '/page2' : (context) =>  Page2(),
        '/page3' : (context) =>  Page3(),
        '/my-post' : (context) =>  MyPost(),
        '/new-post' : (context) =>  NewPost(),
        '/random' : (context) =>  RandomNo(),
        '/guess-game' : (context) =>  GuessGame(),
        '/calculator' : (context) =>  Calculator(),
        '/button-demo' : (context) =>  ButtonDemo(),
        '/list-demo' : (context) =>  ListDemo(),
        '/future-demo' : (context) =>  FutureDemo(),
        '/post-demo' : (context) =>  PostDemo(),
        '/spinkit-demo' : (context) =>  SpinkitDemo(),
        '/worldtime-loading' : (context) =>  Loading(),
        '/worldtime-home' : (context) =>  Home(),
        '/choose-location' : (context) =>  Location(),
        '/snackbar-demo' : (context) =>  SnackBarDemo(),
      },
    )
  );
}
