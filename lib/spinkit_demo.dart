import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class SpinkitDemo extends StatelessWidget {
  const SpinkitDemo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: SpinKitFadingCube(
        color: Colors.redAccent,
        size: 50.0,
      ),
    );
  }
}
