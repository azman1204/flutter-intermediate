import 'package:demo2/snackbar_demo.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'count_controller.dart';

class DialogDemo extends StatelessWidget {
  const DialogDemo({super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(CounterController());
    final CounterController controller = Get.find();

    return GetMaterialApp(
      home: Scaffold(
        appBar: AppBar(title: const Text("Dialog Box Example"),backgroundColor: Colors.brown[700], foregroundColor: Colors.white, centerTitle: true,),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              ElevatedButton(onPressed: (){
                Get.defaultDialog(
                  title: "Demo",
                  middleText: "Hello World From Paris",
                  backgroundColor: Colors.amber
                );
              }, child: const Text("Hello")),
              FloatingActionButton(
                onPressed: (){
                  controller.increment();
                  Get.to(const SnackbarDemo());
                }, child: const Text("+"),
              )
            ],
          ),
        ),
      ),
    );
  }
}
