import 'package:flutter/material.dart';
import 'post_helper.dart';

class PostDemo extends StatefulWidget {
  const PostDemo({Key? key}) : super(key: key);

  @override
  State<PostDemo> createState() => _PostDemoState();
}

class _PostDemoState extends State<PostDemo> {
  List posts = [];

  @override
  void initState() {
    super.initState();
    PostHelper ph = PostHelper();
    //ph.getConnection();
    ph.insertPost();
    ph.updatePost();
    ph.deletePost(2);
    getData(ph);
  }

  void getData(PostHelper ph) async {
    List posts2 = await ph.findAllPost();
    setState(() {
      posts = posts2;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Post List'),
        ),
        body: ListView(
          children: [
            ...posts.map((post) =>
                Card(
                  child: ListTile(
                    title: TextButton(
                      child: Text(post['title']),
                      onPressed: () {
                        updatePost({'title': 'SELESAI:${post['title']}'}, post['id']);
                      },
                    ),
                    subtitle: Text(post['author']),
                    leading: Text(post['id'].toString(),),
                    trailing: IconButton(
                      icon: const Icon(Icons.delete, color: Colors.pink,),
                      onPressed: () {
                        deletePost(post['id']);
                      },
                    ),
                  ),
                ))
          ],
        ),
      ),
    );
  }

  updatePost(Map<String, Object> post, int id) {
    PostHelper ph = PostHelper();
    ph.updatePost2(post, id);
    getData(ph);
  }

  deletePost(int id) {
    PostHelper ph = PostHelper();
    ph.deletePost(id);
    getData(ph);
  }
}

