import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'count_controller.dart';

class SnackbarDemo extends StatelessWidget {
  const SnackbarDemo({super.key});

  @override
  Widget build(BuildContext context) {
    final CounterController controller = Get.find();

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: const Text("Dialog Box Example"),backgroundColor: Colors.brown[700], foregroundColor: Colors.white, centerTitle: true,),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              ElevatedButton(
                onPressed: (){
                  Get.snackbar("Hello", "World..${controller.count}");
                },
                child: const Text("Hello")
              )
            ],
          ),
        ),
      ),
    );
  }
}
